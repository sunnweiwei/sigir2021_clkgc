% !TEX root =  ../main.tex

\section{Introduction}

Recent years have witnessed a rapid development of technology to support open domain human-machine conversations~\citep{Adiwardana2020TowardsAH,Serban2016BuildingED,Vinyals2015ANC,Zhang2020DialoGPTLG,gao-2021-advances-arxiv}.
Although existing models are capable of generating fluent responses based on the conversational history, there is still a clear gap when people converse with such systems, compared with conversations between humans. 
One primary reason is that a lack of proper knowledge in generated responses makes it difficult for conversational methods to dive deeply into a specific topic~\cite{Li2016ADO}.
To bridge this gap, the task of \ac{KGC} has been proposed so as to leverage external knowledge sources to enhance open-domain conversational models~\cite{Dinan2019WizardOW}. \ac{KGC} has seen its first applications to the task of \emph{conversational information retrieval} during the past few years~\cite{chuanmeng2020refnet,ren2020conversations}.

\begin{figure}[h]
 \centering
 \includegraphics[width=1\columnwidth]{figs/fig1_imbalance.pdf}
 \caption{The number of Wikipedia articles in different languages.
 Note that the languages are ranked in descending order of the number of speakers.}
 \label{f1}
\end{figure}

\noindent%
As far as we know, existing \ac{KGC} studies only focus on modeling the knowledge-grounded dialogue scenario with monolingual knowledge. 
However, the amount of knowledge available in different languages is extremely imbalanced.
In Figure~\ref{f1} we list statistics about the number of articles in Wikipedia for various languages.\footnote{\href{https://meta.wikimedia.org/wiki/List\_of\_Wikipedias}{https://meta.wikimedia.org/wiki/List\_of\_Wikipedias}}
We observe an imbalanced distribution over various languages,
% \footnote{\url{https://en.wikipedia.org/wiki/List\_of\_languages\_by\_total\_number\_of\_speakers}} 
e.g., the number of documents in English is 6 times that of Chinese, whereas Urdu only has 160 thousands articles on Wikipedia. 
There is a theoretical possibility to establish an individual large-scale knowledge base for each language to alleviate the knowledge scarcity. But the high cost and effort required make this impractical. 

\begin{figure}[t]
 \centering
 \includegraphics[width=1\columnwidth]{figs/fig2_compare.pdf}
 \caption{(a) Open-domain dialogue grounded by monolingual knowledge (previous work); (b) For knowledge scarce languages, there is a lack of rich knowledge bases; (c) We propose \ac{CKGC} and ground open-domain dialogues in a knowledge scarce language using cross-lingual knowledge.}
 \label{f2}
\end{figure}

To address this problem of limited knowledge sources in some languages, we propose the task of \acp{CKGC} to leverage abundant knowledge in other languages.
Figure~\ref{f2} shows an example of \ac{CKGC}. 
Unlike existing \ac{KGC} approaches, which only work in a monolingual domain, \ac{CKGC} aims to retrieve relevant and accurate sentences from external knowledge in an auxiliary language to improve the informativeness of the response generation in the target language. 
Two main challenges come with the task of \ac{CKGC}:
\begin{enumerate*}
\item searching and representing cross-lingual knowledge; and 
\item the lack of a dataset for evaluation.
\end{enumerate*}
For (1), since it is difficult to construct a large-scale parallel dialogue corpora for training, we have to learn a model to retrieve and express knowledge from an auxiliary language without human annotation. For (2), we need to establish a dataset to evaluate the performance of \ac{CKGC} approaches. 

To tackle the first challenge, we design a two-phase framework for \ac{CKGC}, with a \emph{cross-lingual knowledge retrieval} (CKR) layer and a \emph{multilingual response generation} (MRG) layer. 
On this basis, we propose a \ac{CSKD} scheme. 
With large-scale non-parallel dialogue corpora in both target and auxiliary languages, \ac{CSKD} applies knowledge distillation~\cite{Hinton2015DistillingTK} to improve cross-lingual \ac{KS} and \ac{KE}. To this end, \ac{CSKD} is composed of 3 ingredients: 
\begin{enumerate*}
\item \emph{parallel dialogue mining}, 
\item \emph{self-knowledge distillation}, and 
\item \emph{curriculum learning}. 
\end{enumerate*}
Specifically, 
in \emph{parallel dialogue mining}, we automatically extract pseudo parallel dialogues;
in \emph{self-knowledge distillation}, we distillate the knowledge selection and knowledge expression ability from an auxiliary language to the target language; and
in the \emph{curriculum learning} part, we incorporate curriculum learning~\cite{Bengio2009CurriculumL} into the distillation process to handle the noise caused by automatic parallel dialogue mining.

To tackle the second challenge, we collect a \ac{CKGC} test dataset including about 3,000 conversations in three languages. 
As the first benchmark on the \ac{CKGC} task, our work helps to facilitate relevant research in the future. 

Using our newly created dataset, we conduct extensive experiments to assess the effectiveness of our proposed \ac{CKGC} method. Evaluation results in terms of both automatic metrics and human evaluation indicate that \ac{CSKD} can significantly outperform all baselines in cross-lingual knowledge selection without using any parallel corpus or human annotation. Moreover, we find that \ac{CSKD} increases the topic richness of responses in the target language by leveraging knowledge in an auxiliary language. 

In summary, this paper makes the following contributions:

\begin{itemize}[leftmargin=*,nosep]
\item We are the first to propose the \acf{CKGC} task.
\item We propose a \ac{CSKD} scheme, which learns to search and represent cross-lingual knowledge without annotations.
\item We propose the first cross-lingual knowledge grounded conversational dataset to facilitate research on \ac{CKGC}.
\item We conduct extensive experiments on three languages to empirically validate the effectiveness of the proposed methods.
\end{itemize}
