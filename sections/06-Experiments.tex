% !TEX root =  ../main.tex

\section{Experiments}

\subsection{Research questions}
We aim to answer the following research questions with our experiments:
\begin{enumerate*}[label=(RQ\arabic*)]
\item How does our proposed method, \ac{CSKD}, perform on \ac{CKGC}? Can \ac{CSKD} help to select and express knowledge in an auxiliary language? (See~\S\ref{s07-1} and \S\ref{s07-2}) 
\item What is the effect of each ingredient in \ac{CSKD}? 
(See~\S\ref{s07-3})
\item Can knowledge in an auxiliary language enrich the conversation topics? If so, what is the effect of these topics? (See~\S\ref{s07-4})
\end{enumerate*}
In addition, we show a number of cases to demonstrate the performance of \ac{CSKD}; see~\S\ref{s07-5}.

\subsection{Training data}
We establish the knowledge corpus using a Wikipedia dump,\footnote{\url{https://dumps.wikimedia.org/enwiki/latest/enwiki-latest-pages-articles.xml.bz2}} where text is extracted\footnote{\url{https://github.com/attardi/wikiextractor/wiki}}
and split into sentences using NLTK.\footnote{\url{https://www.nltk.org/}}
In total, there are 6,167,445 articles and 106,824,651 sentences. 
The English dialogue corpus is constructed from the Reddit Conversation Corpus, which contains 20,308,634 conversations. The French dialogue corpus\footnote{\url{https://www.kaggle.com/breandan/french-reddit-discussion}} is collected from Reddit, including 1,026,462 conversations. 
The Chinese dialogue corpus is constructed from an online communication platform, Tieba,\footnote{\url{https://github.com/codemayq/chinese\_chatbot\_corpus}} with 3,059,173 conversations. 
To establish a Spanish dialogue corpus, we collect comments from Reddit during 2019. We first download all the comments submitted in 2019 from the pushshift;\footnote{\url{https://files.pushshift.io/reddit/}} then we identify the language of each comment using a classifier trained by fastText~\cite{Joulin2017BagOT,Grave2018LearningWV}; finally, we match comments based on their parent\_id to construct the context. Our Spanish Reddit dialogue corpus contains 2,012,992 conversations. 
Summary statics of our training datasets are given in Table~\ref{table:training-statistic}.

%  Dataset statistics ================================================
\begin{table}[]
\caption{Statistics of the training data.}
\label{table:training-statistic}
\small
\begin{tabular}{lllll}
\toprule
\textbf{Language}   & English             &Chinese        & French          & Spanish\\ 
\midrule
Crawled from                   & \href{https://github.com/nouhadziri/THRED}{Reddit}      & \href{https://github.com/codemayq/chinese\_chatbot\_corpus}{Tieba} & \href{https://www.kaggle.com/breandan/french-reddit-discussion}{Reddit}  & \href{https://github.com/sunnweiwei/spanish-reddit-dialogues-corpus}{Reddit}      \\
\#Dialogues           & 20,308,634          &3,059,173      & 1,026,462       & 2,012,992    \\
Avg. \#words               & 14.1                & 15.7          & 49.3            &  37.7      \\
Wikipedia       & \multicolumn{4}{l}{\#Sentences: 106,824,651, Avg. \#words: 21.1} \\ 
\bottomrule
\end{tabular}
\end{table}
% ======================================================
% =====================================

\subsection{Baselines}
To verify the effectiveness of \ac{CSKD}, we compare it with the following models\footnote{For a fair comparison, we replaced all baseline backbone networks with mBART except the DialoGPT.}:

\begin{itemize}[leftmargin=*,nosep]
\item \textbf{DialoGPT} \cite{Radford2019LanguageMA} is a dialogue generation model that attains human-close performance in evaluation.
The model follows the architecture of OpenAI GPT-2. 
% The original DialogGPT is in English. 
In our work, we use checkpoints trained in Chinese\footnote{\url{https://github.com/yangjianxin1/GPT2-chitchat}}, French\footnote{\url{https://huggingface.co/antoiloui/belgpt2}}, and Spanish\footnote{\url{https://huggingface.co/datificate/gpt2-small-spanish}}.
We execute the model directly without any fine-tuning.
\item \textbf{mBART} \cite{Liu2020MultilingualDP} is a multilingual model pretrained by denoising auto-encoder objective on the common-crawl-25 corpus; the mbart.cc25 checkpoint was used and we finetune the model on the dialogue corpus shown in Table~\ref{table:training-statistic}.
\item \textbf{Pipeline}~\cite{Chi2020CrossLingualNL} translates input context sentences in a target language into English. Then it performs an English \ac{KGC} model to produce the response, and finally translates it back to the target language.
We use the \ac{KGC} model trained in \S\ref{sec:method:pretrain} and a transformer based model trained on OPUS data~\cite{Tiedemann2020OPUSMTB} for translation.
% Such a translation model achieves BLEU1=36.1, 57.5, 59.6 for Chinese, French, and Spanish Tatoeba~\cite{Artetxe2019MassivelyMS} test sets, respectively. 
\item \textbf{RLKS}~\cite{Zhao2020KnowledgeGroundedDG} supervises \ac{KS} according to the pseudo ground-truth labels they construct and the signals gained in a reinforcement learning way; we use the response translated by the model trained on OPUS data~\cite{Tiedemann2020OPUSMTB} as the query, and employ BM25 to retrieve the pseudo knowledge pool.
\item \textbf{XNLG}~\cite{Chi2020CrossLingualNL} fine tunes a pre-trained language model on English dialogue data first, and directly performs inference on non-English test data in a zero-shot setting. We adopted the \emph{Fine-Tuning for Any-to-Others NLG} strategy in \cite{Chi2020CrossLingualNL}, that keeps the decoder and the word embeddings frozen and only updates the encoder parameters during fine-tuning, to avoid catastrophic forgetting of target language controllability.
\end{itemize}

\subsection{Evaluation metrics}
We use Recall@1 as automatic metric to evaluate the cross-lingual knowledge retrieval task. 
To evaluate response generation, we choose unigram F1\footnote{\url{https://github.com/facebookresearch/ParlAI/blob/master/parlai/core/metrics.py}} and ROUGE-1/2~\cite{Lin2004ROUGEAP} as automatic metrics. 
We also assess the performance of all methods in terms of human annotations by following~\cite{Li2020ZeroResourceKD}. 
We randomly sample 300 dialogues and their corresponding generations from our model as well as the baselines. We recruit 6 experienced translation experts as annotators. 
Specifically, given the conversation context, the knowledge pool used at the current turn, the selected knowledge, as well as the generated responses, each expert needs to give a preference~(i.e., ``bad'', ``fair'', and ``good'') in terms of three aspects: \emph{fluency}, \emph{context coherence}, and \emph{knowledge relevance}.
Fluency measures if the generated response is smooth; 
context coherence measures if the generated response and dialogue context are coherent;
and knowledge relevance measures if the selected knowledge and dialogue context are relevant.
Each response receives 3 scores per aspect, and agreement among the annotators is measured via Fleiss’ kappa~\cite{Fleiss1971MeasuringNS}.

\subsection{Implementation details}
%Although more delicate methods can be applied~\cite{Kim2020SequentialLK}, we concatenate the conversation history into a long sentence with $[SEP]$ token as context for simplicity. 
In the inference phase, our model uses the knowledge pool in the dataset (see \S\ref{sec:dataset} for the construction method) just like the baselines.
% In the warm-up stage in section \ref{sec:method:warmup}, we retrieve the sentences in Wikipedia via an open source Apache Solr,\footnote{\url{https://lucene.apache.org/solr/}} and we employ the internal ranker of Lucene (basically a BM25 model~\cite{Robertson1994OkapiAT}). 
We use FAISS~\cite{Johnson2017BillionscaleSS} to accelerate the dense vectors index process. We use mbart.cc25 checkpoint~\cite{Liu2020MultilingualDP} (680M parameters)\footnote{\url{https://github.com/pytorch/fairseq/tree/master/examples/mbart}} as the backbone network of our model and all the baselines. The model is trained on 4 NVIDIA TITAN RTX GPUs, with a $\mathit{batch~size}=64$. We set the maximum input length as 128, and a maximum output length as 64. We set the knowledge pool size as 10. All models are optimized using the AdamW optimizer with $lr=2e-5$, $\beta1=0.9$, and $\beta2=0.999$. During decoding, we use beam search algorithm and set $\mathit{beam~size}=3$.
