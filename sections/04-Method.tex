% !TEX root =  ../main.tex

\begin{figure*}[t]
 \centering
 \includegraphics[width=1\textwidth]{figs/fig3_method.pdf}
 \caption{An overview of \ac{CKGC} framework and \ac{CSKD} scheme.}
 \label{figure2}
\end{figure*}

\section{Method}
We first introduce the neural parameterization method of two layers in Section~\ref{sec:method:parameterization}. 
Then in Section~\ref{sec:method:pretrain}, we introduce the pre-training method on an auxiliary language. 
After that, we detail \ac{CSKD} in Section~\ref{sec:method:cskd}, which learns $P(K|C^{T})$ and $P(R^{T}|C^{T},K)$ in an unsupervised manner. 

\subsection{Neural parameterization}
\label{sec:method:parameterization}

\textbf{Cross-lingual knowledge retrieval layer.}
Given a conversation $(C^{T/A},R^{T/A})$ in language $T$ or $A$, and a large knowledge corpus $\mathcal{K}$, we first use a transformer encoder with parameters $\theta$ to encode the context sentence $C$, so we have:
%
\begin{equation}
\label{eq:encode}
\mathbf{h}^{C^{T/A}} = \operatorname{L2Norm}(\operatorname{AvgPooling}(\operatorname{Encoder}(C^{T/A}, \theta))) \in \mathbb{R}^{1 \times d},
\end{equation}
%
where $\mathbf{h}^{C^{T/A}} \in \mathbb{R}^{1 \times d}$ refers to the sentence representation with hidden size $d$, $\operatorname{L2Norm}$ indicates a L2-normalization operation, and $\operatorname{AvgPooling}$ means average-pooling.
Likewise, we encode all the knowledge sentences in $\mathcal{K}$ to $\mathbf{h}^{\mathcal{K}} \in \mathbb{R}^{|\mathcal{K}| \times d}$ using the same method.
%perform two-step selection on $\mathcal{K}$. We
Then, we construct the knowledge pool $KP(C^{T/A})$ using the nearest neighbors of $C^{T/A}$:
%
\begin{equation}
\label{eq:knn}
KP(C^{T/A}) = \operatorname{KNN}( \mathbf{h}^{C^{T/A}},\mathbf{h}^{\mathcal{K}} ),
\end{equation}
%
where $\operatorname{KNN}$ denotes a K Nearest Neighbors function using cosine similarity. We use FAISS~\cite{Johnson2017BillionscaleSS} to simultaneously search neighborhoods for all dialogues in an efficient manner. 

Given the knowledge pool $KP(C^{T/A})$, we use an attention mechanism to perform a fine-grained selection of which knowledge sentences are used to generate the response:
%
\begin{equation}
\label{eq:selection}
\begin{split}
P( K | C^{T/A},KP(C^{T/A}) ) = \frac{\exp( \mathbf{h}^{C^{T/A}} \cdot {\mathbf{h}^{K}}^\top )}{{\sum_{\dot{K} \in KP(C^{T/A})}{\exp( \mathbf{h}^{C^{T/A}} \cdot {\mathbf{h}^{\dot{K}}}^\top )}}}.
\end{split}
\end{equation}
%
When training the model, we build the knowledge pool $KP(R^{T/A})$ by replacing $C^{T/A}$ with response $R^{T/A}$.

\noindent \textbf{Multilingual response generation layer.}
Given multilingual inputs, we employ a transformer encoder-decoder network with parameters $\phi$ in the \ac{MRG} layer to generate the response. 
Specifically, we first concatenate $C^{T/A}$ and the selected knowledge $K$ obtained from the \ac{CKR} layer with a $[SEP]$ token to get the input, i.e., $I=\{K;[SEP];C^{T/A}\}$.
%\footnote{We follow~\cite{Karthikeyan2020CrossLingualAO} to remove the $[LANG]$ token from the encoder input, as we have input with multiple languages}
Then we encode $I$ into a latent representation $\mathbf{H}^{I} \in \mathbb{R}^{|I| \times d}$, where $|I|$ denotes the number of tokens in $I$ and $d$ denotes the hidden size. 
In the decoding stage, we decode to generate the response token by token with a start language identification $[LANG]$. 
Concretely, the probability of generating token $R_{t}$ from a predefined vocabulary $V$ at the timestamp $t$ is defined as:
%
\begin{equation}
P( R_{t}^{T/A} | R_{1:t}^{T/A},C,K ) = \operatorname{Softmax}( \mathbf{h}_{(n)}^{R_{t}^{T/A}} \cdot \mathbf{E}^\top ) \in \mathbb{R}^{1 \times |V|},
\end{equation}
%
\noindent where $\mathbf{E} \in \mathbb{R}^{|V| \times d}$ denotes an embedding matrix of vocabulary $V$; $\mathbf{h}_{(n)}^{R_{t}^{T/A}} \in \mathbb{R}^{d}$ denotes $d$-dimension word representations in $R_{t}$ for a decoder with \emph{n} layers. Thus, for the $l$-th layer in the decoder, we have:
%
\begin{equation}
\begin{split}
\mathbf{h}_{(l)}^{R_{t}^{T/A}} & = \operatorname{FFN}( \operatorname{LN}( \mathbf{A}_{(l)}^{R_{t}^{T/A}} \cdot {\mathbf{H}^{I}}^\top + \mathbf{s}_{(l)}^{R_{t}^{T/A}} ) ) \in \mathbb{R}^{1 \times d},
\\
\mathbf{A}_{(l)}^{R_{t}^{T/A}} & = \operatorname{Softmax}( \mathbf{s}_{(l)}^{R_{t}^{T/A}} \cdot {\mathbf{H}^{I}}^\top ) \in \mathbb{R}^{1 \times |I|},
\\
\mathbf{s}_{(l)}^{R_{t}^{T/A}} & = \operatorname{LN}( \operatorname{SA}( \mathbf{h}_{(l-1)}^{R_{t}^{T/A}},{\mathbf{H}_{(l-1)}^{R_{1:t}^{T/A}}}\top ) + \mathbf{h}_{(l-1)}^{R_{t}^{T/A}} ) \in \mathbb{R}^{1 \times d},
\end{split}
\end{equation}
%
where $\mathbf{A}^{R_{t}^{T/A},(l)}$ denotes the attention distribution at the $l$-th layer, which represents the encoder inputs during the generation process; $\operatorname{LN}$ denotes the layer normalization operation, whereas $\operatorname{FFN}$ is a position-wise fully connected feed-forward
network with GeLU non-linear activation;
$\mathbf{H}_{(l-1)}^{R_{1:t}^{T/A}} = \{ \mathbf{h}_{(l-1)}^{R_{\tau}^{T/A}} \}_{\tau = 0}^{t} \in \mathbb{R}^{t \times d}$ denotes a representation matrix of $R_{1:t}$ at the $l$-th layer. 
Following~\cite{Vaswani2017AttentionIA}, we write $\operatorname{SA}$ for the self attention block.

% Algorithm =======================================
\begin{algorithm}[t]
% \label{algorithm}
\caption{CSKD training}
\label{algorithm}
\begin{algorithmic}[1]
\State \textbf{Input:} dialogue corpus $\mathcal{D}^{A}$ in auxiliary language, dialogue corpus $\mathcal{D}^{T}$ in target language, knowledge corpus $\mathcal{K}$, pre-trained mBART, maximum step M
\State Initialize parameters $\theta$ of \ac{CKR} and $\phi$ of \ac{MRG} with mBART
\State Pre-train $\theta$ and $\phi$ on auxiliary language \Comment{See \S\ref{sec:method:pretrain}}
\State Construct pseudo parallel dialogue $\mathcal{D}_{PPD}$.\Comment{See \S\ref{sec:method:cskd:parallel}} 
\State Sort $\mathcal{D}_{PPD}$ according to Eq.~\ref{eq:d}
\For{training step m = 1, ..., M}
\State Sample a batch $B_{m}$ in $\mathcal{D}_{PPD}$ based Eq.~\ref{eq:f};
\State Sample $\{(C^{T},R^{T}),(C^{A},R^{A})\}$ from $B_{m}$;
\State Update $\theta$ based on Eq.~\ref{eq:ksd};
\State Update $\phi$ based on Eq.~\ref{eq:ked} and Eq.~\ref{eq:generation};
\EndFor 
\State\textbf{end for} 

\end{algorithmic}
\end{algorithm}
% END Algorithm =======================================

\subsection{Pretraining on an auxiliary language}
\label{sec:method:pretrain}
In this stage, we aim to pre-train the network in auxiliary language given dialogues $\mathcal{D}_{A}$ and a knowledge corpus $\mathcal{K}$.
Inspired by~\citet{Zhao2020KnowledgeGroundedDG}, we pre-train the network auxiliary language use a two-stage paradigm: warm-up stage and reinforcement learning stage.

\subsubsection{Warm-up stage}
\label{sec:method:warmup}
We first warm up the network by collecting pseudo ground truth~\cite{Zhao2020KnowledgeGroundedDG}. 
Specifically, we construct the pseudo knowledge pool $\hat{KP(R^{A})}$ and pseudo ground truth knowledge $\hat{K}$ by BM25 using responses as queries. 
With the $\hat{KP(R^{A})}$ and $\hat{K}$, \ac{CKR} are optimized via the maximum likelihood estimation:
%
\begin{equation}
\begin{split}
\mathcal{L}_{CKR} = {\sum_{(C^{A},R^{A}) \in \mathcal{D}_{A}}{-\log(P( \hat{K} | R^{A},\hat{KP(R^{A})} ))}},
\\
\end{split}
\end{equation}
%
and MLG layers are optimized via NLL loss:
%
\begin{equation}
\mathcal{L}_{MLG} = {\sum_{(C,R) \in \mathcal{D}_{A}}{-\log(P(R^{A}|C^{A},\hat{K} ))}},
\end{equation}

\subsubsection{Reinforcement learning stage}
After the warm-up stage, the CKR layer is further improved via the policy gradient~\cite{Sutton1999PolicyGM} with a reward function from the MRG layer. 
We draw knowledge $K$ from $P(K|C^{A}, \hat{KP(R^{A})})$ and define the reinforcement learning objective as follows:
\begin{equation}
\label{eq:41}
\mathcal{L}_{RL} = -{\sum\limits_{(C^{A},R^{A}) \in \mathcal{D}_{A}}{\mathbb{E}_{K\sim P(K|\cdot)}~\overset{\sim}{\mu}_{(R^{A},C^{A},K)} \cdot \log( P( K | \cdot ) )}},
\end{equation}
where
\begin{equation}
\label{eq:42}
\begin{split}
\overset{\sim}{\mu}_{(R^{A},C^{A},K)} & = \mu_{(R^{A},C^{A},K)} - b
\\
\mu_{(R^{A},C^{A},K)} & = P( R^{A} | C^{A},K ) ^ {\varepsilon/|R^{A}|}.
\end{split}
\end{equation}
%
In Eq.~\ref{eq:41} and~\ref{eq:42}, we write $P(K|\cdot)$ as a shorthand for $P(K|C^{A},\hat{KP(R)})$, $\varepsilon$ is the temperature, whereas $|R^{A}|$ is the length of the response. 
Following~\cite{Clark2016DeepRL}, we write $b$ for the baseline to reduce the variance of gradient estimation, so we have:
 %
\begin{equation}
b=\frac{1}{|\hat{KP(R^{A})}|}\sum_{\dot{K} \in \hat{KP(R^{A})}}{\mu_{(R^{A},C^{A},\dot{K})}},
\end{equation}
where $\mu_{(R^{A},C^{A},K)}$ is the reward function given context $C^{A}$, knowledge $K$, and the target response $R^{A}$.
% 

\subsection{Curriculum self-knowledge distillation}
\label{sec:method:cskd}

After pretraining, our method is able to select and express the knowledge in an auxiliary language. 
However, the cross-lingual knowledge selection and expression is still limited. 
By assuming that a parallel dialogue has similar knowledge expression, we propose a \ac{CSKD} scheme in order to distillate knowledge from an auxiliary language to a target language. 
The learning algorithm of \ac{CSKD} is summarized in Algorithm~\ref{algorithm}.
CSKD is composed of three ingredients:
(1) \emph{parallel dialogue mining}: we extract pseudo parallel dialogues automatically; 
(2) \emph{distillation on knowledge selection and expression}: we distillate the knowledge selection and expression abilities from auxiliary language to target language; and
(3) \emph{curriculum learning}: we incorporate the curriculum learning into the distillation to handle the noise in the parallel dialogues mining stage.
%Next, we introduce the details of the three ingredients.

% \noindent \textbf{Parallel dialogue mining}
\subsubsection{Parallel dialogue mining}
\label{sec:method:cskd:parallel}

In this part, we extract pseudo parallel dialogues and construct $\mathcal{D}_{PPD}=\{ (C_{i}^{T},R_{i}^{T}),(C_{j}^{A},R_{j}^{A}) \}_{i=1}^{|\mathcal{D}_{T}|}$, where $\forall i$, $(C_{j}^{A},R_{j}^{A})$ is the pseudo parallel dialogue of $(C_{i}^{T},R_{i}^{T})$. 

To mine parallel dialogues without supervised signals, we first encode each response $R_{i}^{T}$ in $\mathcal{D}^{T}$ into a representation $\mathbf{h}^{R_{i}^{T}}$ via Eq.~\ref{eq:encode}, and each $R_{j}^{A}$ in the auxiliary language is encoded into $\mathbf{h}^{R_{j}^{A}}$ in the same way. 
Let $\mathbf{h}^{\mathcal{D}_{T}}=\{\mathbf{h}^{R_{i}^{T}}\}_{i=1}^{|\mathcal{D}_{T}|} \in \mathbb{R}^{|\mathcal{D}_{T}| \times d}$ and $\mathbf{h}^{\mathcal{D}_{A}}=\{\mathbf{h}^{R_{i}^{A}}\}_{i=1}^{|\mathcal{D}_{A}|} \in \mathbb{R}^{|\mathcal{D}_{A}| \times d}$ be the encoded dialogue corpus in target and auxiliary languages, respectively.

Then, we use the same KNN function in Eq.~\ref{eq:knn} to find $N$ nearest neighbors of each dialogue in the target language. 
Therefore, for the $i$-th dialogue in the target language $(C_{i}^{T},R_{i}^{T})$, we have $\operatorname{NN}(R_{i}^{T})=\operatorname{KNN}(\mathbf{h}^{R_{i}^{T}}, \mathbf{h}^{\mathcal{D}_{A}})$.
Given a pair of dialogues in the target and auxiliary languages, i.e., $(C_{i}^{T}, R_{i}^{T})$ and $(C_{j}^{A}, R_{j}^{A})$, we calculate their semantic similarity by a ratio margin function~\cite{Artetxe2019MarginbasedPC}:
%
\begin{equation}
\label{ep:margin}
\begin{split}
\operatorname{S}( i,j )  = & \cos( \mathbf{h}^{R_{i}^{T}},\mathbf{h}^{R_{j}^{A}} )/
\\
& \left[{\sum_{\mathbf{z} \in \operatorname{NN}(R_{i}^{T})}{\cos( \mathbf{h}^{R_{i}^{T}},\mathbf{z} )}} + {\sum_{\mathbf{z} \in \operatorname{NN}(R_{j}^{A})}{\cos( \mathbf{z},\mathbf{h}^{R_{j}^{A}} )}}\right],
\end{split}
\end{equation}
%
where $\mathbf{z} \in \mathbb{R}^{1 \times d}$, $\mathbf{z} \in \operatorname{NN}(R_{i/j}^{T/A}) $ denotes the representation of a dialogue in $\operatorname{NN}(R_{i/j}^{T/A})$.
Next, we construct the pseudo parallel dialogue for each $(C_{i}^{T},R_{i}^{T}) \in \mathcal{D}^{T}$ using the dialogue $(C_{j}^{A},R_{j}^{A})$ with the highest $\operatorname{S}(i,j)$.

\subsubsection{Distillation on knowledge selection and expression}
\label{sec:cskd:distillation}

We propose two distillation objectives, \ac{KSD} and \ac{KED}, in order to distill the knowledge selection and expression abilities from the auxiliary language to target language.
Specifically, given parallel dialogues $\{( C^{T},R^{T} ),( C^{A},R^{A} )\}$, we define the objective function of \ac{KSD} as:
%
\begin{equation}
\label{eq:ksd}
\mathcal{L}_{KSD} = {\sum_{K \in KP}{P( K | R^{A},KP )\log\left( \frac{P( K | R^{A},KP )}{P( K | C^{T},KP )} \right)}},
\end{equation}
%
where the knowledge pool $KP$ denotes $KP(R^{A})$ constructed by the response in the auxiliary language. 
The objective function of \ac{KED} is defined as:
%
\begin{equation}
\label{eq:ked}
\begin{split}
\mathcal{L}_{KED} & = {\sum\limits_{l = 1}^{n}{1/n~\operatorname{KL}( \operatorname{AP}( \mathbf{A}_{(l)}^{R^{A}} ),\operatorname{AP}( \mathbf{A}_{(l)}^{R^{T}} ) )}},
\\
\mathbf{A}_{(l)}^{R} & = \{ \mathbf{A}_{(l)}^{R_{\tau}} \}_{\tau = 1}^{|R|} \in \mathbb{R}^{|R| \times |I|},
\end{split}
\end{equation}
%
where $n$ is the number of layers in decoder;
$I$ is the concatenation of context and knowledge sequences;
%, we sample a piece of knowledge from $P(K|R^{A},KP)$ as the knowledge to be used for both the dialogue in auxiliary language and in target language;
$\operatorname{KL}$ denotes the Kullback-Leibler divergence;
$A_{(l)}^{R} \in \mathbb{R}^{|R| \times |I|}$ denotes the attention matrix of response $R$ attending to encoder input $I$ in the $l$-th decoder layer; we apply an average pooling operation $\operatorname{AP}$ on the attention matrix and obtain $\operatorname{AP}( \mathbf{A}_{(l)}^{R} ) \in \mathbb{R}^{1 \times |I|}$, where
we mask the context part in $\operatorname{AP}( \mathbf{A}_{(l)}^{R} )$ to ensure that the two input variables of $\operatorname{KL}(\cdot,\cdot)$ have the same size. 
The masked $\operatorname{AP}( \mathbf{A}_{(l)}^{R} )$ indicates the average intensity of attention of each knowledge token during the response generation process.
%, which can represents the expression of the knowledge. 
Besides, we optimize the \ac{MLG} via an additional NLL loss:
%
\begin{equation}
\label{eq:generation}
\mathcal{L}_{g} = {-\log(P(R^{T}|C^{T},K ))},
\end{equation}
%
where $K \sim P( K | R^{A},KP(R^{A}) )$ is a piece of knowledge chosen by $R^{A}$.
%as the pseudo ground truth knowledge.

% \noindent \textbf{Curriculum training scheduler}
\subsubsection{Curriculum learning}
\label{sec:cskd:curriculum}
In order to reduce the impact of noise caused in parallel dialogues mining, we introduce curriculum learning \cite{Bengio2009CurriculumL} into the distillation process. 
We design a curriculum training scheduler to provide the model with easy samples first, then gradually increase the difficulty of samples. 
The curriculum training scheduler is arranged by sorting each pseudo parallel dialogue according to the difficulty defined as follows:
\begin{equation}
\label{eq:d}
d(i,j)=\operatorname{S}(i,j)P(K_{best}|R_{j}^{A},KP(R_{j}^{A})),
\end{equation}
where the lower $d(i,j)$ is, the more difficult the pseudo parallel dialogue is; $\{( C_{i}^{T},R_{i}^{T} ),( C_{j}^{A},R_{j}^{A} )\}$ is a pseudo parallel dialogue from $\mathcal{D}_{PPD}$;
the function $\operatorname{S}(\cdot,\cdot)$ is defined in Eq.~\ref{ep:margin}; 
the function $P(\cdot|\cdot,\cdot)$ is defined in Eq.~\ref{eq:selection};
and $K_{best}$ is the knowledge chosen from $KP(R_{j}^{A})$ by $R_{j}^{A}$.

During training, the model will first train on data with lower difficulty according to the training scheduler, and gradually increase the proportion of difficult data until all the data is used.
At training step $m$, a batch of training samples is obtained from the top $\operatorname{f}(m)$ part of the entire sorted training samples. Following \cite{Platanios2019CompetencebasedCL}, we define the function $f(m)$ as:
\begin{equation}
\label{eq:f}
f(m) \triangleq \operatorname{min}\left( 1,\sqrt{\frac{m( 1 - \alpha_{0}^{2} )}{M} + \alpha_{0}^{2}} \right).
\end{equation}
where $\alpha_{0}^{2}$ denotes the percentage of data used in the initial training stage, $M$ is the maximum step. 
